#!/usr/bin/python
"""	
	Python version: 2.7.1
	Description: Python script, intended to search through all or specific MySQL databases for patterns or strings. Based on the script https://code.google.com/archive/p/anywhereindb/.
	Files required: You should create a csv file named "search_terms.csv" with the format "ID;search_term".
	Output: File named "matches.csv" which contains the following information by line:
		Database name;table searched;field searched;search_term;coincidence.
	Usage: python anywheredb_python.py -h
"""

__author__ = 'Ior4'
__email__ = 'ior4nam@gmail.com'
__version__ = '1.1.0'

import argparse
import csv
import MySQLdb
import os
import os.path

LOGGING_FILE = 'matches.csv'
SEARCH_FILE = 'search_terms.csv'

def main():
	options = get_options()
	print("Loading search terms.")
	search_terms = get_search_terms()
	print("Search terms loaded.")
	print("Searching.")
	search_databases(search_terms,options)
	print("Search done.")
	print("Program finished.")

def get_options():
	parser = argparse.ArgumentParser()
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument('--all', action='store_true', dest='all_databases', help='Indicates if the script must search in all databases')
	group.add_argument('-d', nargs='+', dest='databases_for_search', help='Add databases within to search separated by commas [i.e. db1,db2,db3]')
	parser.add_argument('--host', action='store', dest='mysql_host', help='Hostname or IP where is stored the database', required=True)
	parser.add_argument('--user', action='store', dest='mysql_user', help='Username used to connect to the database', required=True)
	parser.add_argument('--passwd', action='store', dest='mysql_passwd', help='Password used to connect to the database', required=True)
	return parser.parse_args()

def get_search_terms():
	with open(SEARCH_FILE,'rb') as csvfile:
		return list(csv.reader(csvfile, delimiter = ';'))
        
def search_databases(list_of_search_terms,options):
	connection = MySQLdb.connect(
			host = options.mysql_host,
			user = options.mysql_user,
			passwd = options.mysql_passwd)

	if os.path.isfile(LOGGING_FILE):
		os.remove(LOGGING_FILE)

	cursor = connection.cursor()
	if options.all_databases:
		cursor.execute("SHOW DATABASES")
		db_tmp = cursor.fetchall()
		databases = []
		i = 0
		for db_field_0 in db_tmp:
			databases.append(db_tmp[i][0])
			i += 1
	else:
		databases = options.databases_for_search

	print "Searching within the following databases:"
	print "-----------------------------------------"
	for db in databases:
		print db
	print "-----------------------------------------"
	
	for tts in list_of_search_terms:
		for database in databases:
			cursor.execute("USE "+ database)
			cursor.execute("SHOW TABLES")
			tables = cursor.fetchall()
			for table in tables:
				cursor.execute("DESC "+ "`" +table[0]+ "`")
				fields_table = cursor.fetchall()
				for field in fields_table:
					if 'varchar' in field[1]:
						cursor.execute("SELECT `"+field[0]+"` FROM `"+table[0]+"` WHERE `"+field[0]+"` like '%"+tts[1]+"%' LIMIT 0,2")
						query_result = cursor.fetchall()
						if query_result:
							cursor.execute("SELECT * FROM `"+table[0]+"` WHERE `"+field[0]+"` like '%"+tts[1]+"%'")
							total_query_result = cursor.fetchall()
							f = open(LOGGING_FILE,'a')
							for tqr in total_query_result:
								f.write(database+';'+table[0]+';'+field[0]+';'+tts[1]+';'+'--SEP--'.join(str(i) for i in tqr)+os.linesep)
								print("Coincidence at: "+database+';'+table[0]+';'+field[0]+';'+tts[1]+';'+'--SEP--'.join(str(i) for i in tqr))
							f.close()
			
if __name__ == "__main__":
	main()
