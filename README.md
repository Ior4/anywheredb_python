# README #
Python version: 2.7.1 

Description: Python script, intended to search through all or specific MySQL databases for patterns or strings. Based on the script https://code.google.com/archive/p/anywhereindb/.

Files required: You should create a csv file named "search_terms.csv" with the format "ID;search_term". 

Output: File named "matches.csv" which contains the following information by line: 
	Database name;table searched;field searched;term_to_search;coincidence(each field separated by "--SEP--" separator).

Other requirements: You should install MySQLdb. If you get stuck, you can follow these instructions http://stackoverflow.com/questions/25865270/how-to-install-python-mysqldb-module-using-pip.

Usage: python anywheredb_python.py -h

If you wish, you can contact me at ior4nam@gmail.com